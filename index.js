
fetch('https://restcountries.com/v2/all?fields=name,region')
.then(res => res.json())
.then(res => toDisplayData(res))



function toDisplayData (countryData) {
    const DropDown = document.getElementById('regionWiseCountry')
    DropDown.addEventListener('click', () => {
    const continentCountry = document.getElementById('continentCountry')
    continentCountry.innerHTML = '';
    countryData.forEach(countryInfoObj => {
        if(countryInfoObj.region == DropDown.value){
            const li = document.createElement('li')
            li.append(countryInfoObj.name);
            const ul = document.getElementById('continentCountry')
            ul.appendChild(li)
        }
        
    });


})
}

